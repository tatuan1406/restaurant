//
//  CocktailTableViewCell.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/18/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import SDWebImage

class CocktailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_cocktail: UIImageView!
    
    @IBOutlet weak var lblName_CockTail: UILabel!
    
    @IBOutlet weak var lblGlass: UILabel!
    
    @IBOutlet weak var lblInstructions: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_cocktail.CircleImage()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func commonInfor(_ img_Cocktail: String, lblnameCockTail :String, lblnameGlass: String, lblDetail: String ){
        
        //đưa đoạn mã xuống dưới background để load ảnh - phù hợp với đoạn code defual Apple.
//                DispatchQueue.global().async(){
//                    if let url = URL(string: img_Cocktail){
//                        do{
//                            let data = try Data(contentsOf: url)
//                            DispatchQueue.main.async {
//                                self.img_cocktail.image = UIImage(data: data)
//                            }
//                        }catch let err{
//                            print("Error is: \(err.localizedDescription)")
//                        }
//                    }
//                }
        img_cocktail.sd_setImage(with: URL(string: img_Cocktail))
        lblName_CockTail.text = lblnameCockTail
        lblGlass.text = lblnameGlass
        lblInstructions.text = lblDetail
        
    }
    
}
