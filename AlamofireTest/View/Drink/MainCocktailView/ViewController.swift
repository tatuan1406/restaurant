//
//  ViewController.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/18/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let presenterCocktail = PresenterCocktail()
    
    @IBOutlet weak var tableViewCocktail: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewCocktail.delegate = self
        tableViewCocktail.dataSource = self
        
        presenterCocktail.cocktailDelegate = self
        
        presenterCocktail.getDataFromDB()
        
        registerNib()
        
    }
    
    func registerNib(){
        let cocktailNib = UINib(nibName: "CocktailTableViewCell", bundle: nil)
        tableViewCocktail.register(cocktailNib, forCellReuseIdentifier: "CocktailTableViewCell")
    }
    
}

extension ViewController: DelegateCocktail{
    
    
    func getData() {
        
        tableViewCocktail.reloadData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenterCocktail.countAPI
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CocktailTableViewCell", for: indexPath) as! CocktailTableViewCell
        
        cell.commonInfor(presenterCocktail.ArrayThumb[indexPath.row], lblnameCockTail: (presenterCocktail.ArrayName[indexPath.row]), lblnameGlass: (presenterCocktail.ArrayGlass[indexPath.row]), lblDetail: (presenterCocktail.ArrayInstruction[indexPath.row]))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let roationTransform = CATransform3DTranslate(CATransform3DIdentity, -30, 10, 0)
        cell.alpha = 0
        cell.layer.transform = roationTransform
        UIView.animate(withDuration: 0.75) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let vc: DetailCockTailViewController = storyboard.instantiateViewController(withIdentifier: "DetailCocktailViewController") as? DetailCockTailViewController {
            
            vc.getDataForDetail(name: presenterCocktail.ArrayName[indexPath.row], glass: presenterCocktail.ArrayGlass[indexPath.row], instruction:  presenterCocktail.ArrayInstruction[indexPath.row], thumb: presenterCocktail.ArrayThumb[indexPath.row], alcoholic: presenterCocktail.ArrayAlcoholic[indexPath.row], category: presenterCocktail.ArrayCategory[indexPath.row])
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
