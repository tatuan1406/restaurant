//
//  DetailCockTaiViewController.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/20/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import SDWebImage

class DetailCockTailViewController: UIViewController {
    
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var lblNameDetail: UILabel!
    @IBOutlet weak var lblCategoryDetail: UILabel!
    @IBOutlet weak var lblGlassTypeDetail: UILabel!
    @IBOutlet weak var lblAlcoholicDetail: UILabel!
    @IBOutlet weak var lblInstructionDetail: UILabel!
    
    private let presenterDetail = PresenterDetailCocktail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgDetail.CircleImage()
        
        presenterDetail.delegateDetailCocktail = self
        
        setDataToView()
       
    }
    
    func getDataForDetail(name: String, glass: String, instruction: String, thumb: String, alcoholic: String, category: String) {
        presenterDetail.getDataForDetail(name: name, glass: glass, instruction: instruction, thumb: thumb, alcoholic: alcoholic, category: category)
    }
    
    func setDataToView(){
        imgDetail.sd_setImage(with: URL(string: presenterDetail.Thumb ?? "miss"))
        lblNameDetail.text = presenterDetail.Name ?? "miss"
        lblCategoryDetail.text = presenterDetail.Category ?? "miss"
        lblGlassTypeDetail.text = presenterDetail.Glass ?? "miss"
        lblAlcoholicDetail.text = presenterDetail.Alcoholic ?? "miss"
        lblInstructionDetail.text = presenterDetail.Instruction ?? "miss"
    }
    
    @IBAction func backToMain(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
extension DetailCockTailViewController: DelegateDetailCocktail{
    
}
