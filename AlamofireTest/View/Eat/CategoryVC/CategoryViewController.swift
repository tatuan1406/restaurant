//
//  CategoryViewController.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/21/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    @IBAction func Search(_ sender: Any) {
        
    }
    @IBOutlet weak var eat_collection: UICollectionView!
    
    let presenterCategory = PresenterCategory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenterCategory.delegateCategory = self
        
        eat_collection.delegate = self
        eat_collection.dataSource = self
        
        //registerNib
        let categoryNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        eat_collection.register(categoryNib, forCellWithReuseIdentifier: "CategoryCollectionViewCell")
        
        presenterCategory.getDataCategory()
    }
    
}
extension CategoryViewController: DelegateCategory{
    func putDataCategory() {
        eat_collection.reloadData()
    }
    
}
extension CategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(presenterCategory.countCategory)
        return presenterCategory.countCategory 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        categoryCell.addDataCell(presenterCategory.ArraystrCategoryThumb[indexPath.row], lbl_cat_name: presenterCategory.ArraystrCategory[indexPath.row], lbl_cat_dec: presenterCategory.ArraystrCategoryDescription[indexPath.row])
        return categoryCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width - 10)/2
        return CGSize.init(width: width , height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let roationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 10, 0)
        cell.alpha = 0
        cell.layer.transform = roationTransform
        UIView.animate(withDuration: 0.75) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let FoodVC: FoodViewController = storyboard.instantiateViewController(withIdentifier: "FoodVC") as? FoodViewController {
            
            FoodVC.getKeyword(keyword: presenterCategory.ArraystrCategory[indexPath.row])
            
            self.navigationController?.pushViewController(FoodVC, animated: true)
        }
    }
}

