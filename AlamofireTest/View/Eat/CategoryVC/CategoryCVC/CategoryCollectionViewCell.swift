//
//  CategoryCollectionViewCell.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/21/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_Category: UIImageView!
    
    @IBOutlet weak var lbl_category_name: UILabel!
    
    @IBOutlet weak var lbl_category_decription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func addDataCell(_ img_cat: String, lbl_cat_name: String, lbl_cat_dec: String){
        img_Category.sd_setImage(with: URL(string: img_cat))
        lbl_category_name.text = lbl_cat_name
        lbl_category_decription.text = lbl_cat_dec
    }
    
}
