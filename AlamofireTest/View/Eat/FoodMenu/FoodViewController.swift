//
//  FoodViewController.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {
    
    @IBOutlet weak var food_tableView: UITableView!
    
    let presenterFood:PresenterFood = PresenterFood()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        food_tableView.delegate = self
        food_tableView.dataSource = self
        
        self.navigationItem.title = "Food Menu"
        
        presenterFood.delegateFood = self
        
        let foodNib = UINib(nibName: "FoodTableViewCell", bundle: nil)
        food_tableView.register(foodNib, forCellReuseIdentifier: "FoodTableViewCell")
        
        presenterFood.getDataWithKeyword()
        food_tableView.separatorStyle = .none
    }
    
    func getKeyword(keyword: String){
        presenterFood.getKeyword(keyword: keyword)
    }
}
extension FoodViewController: DelegateFood{
    func didGetData() {
        food_tableView.reloadData()
    }
    
}
extension FoodViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenterFood.foodModel?.meals?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let foodCell = tableView.dequeueReusableCell(withIdentifier: "FoodTableViewCell") as! FoodTableViewCell
        foodCell.setDataForFood((presenterFood.foodModel?.meals![indexPath.row].strMealThumb)!, lbl_name: (presenterFood.foodModel?.meals![indexPath.row].strMeal)!, lbl_money: "$ \(presenterFood.foodModel?.meals![indexPath.row].idMeal! ?? "")")
        return foodCell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let roationTransform = CATransform3DTranslate(CATransform3DIdentity, -30, 10, 0)
        cell.alpha = 0
        cell.layer.transform = roationTransform
        UIView.animate(withDuration: 0.75) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let width = (tableView.bounds.size.width)
        return CGFloat.init(width / 3)
    }
}
