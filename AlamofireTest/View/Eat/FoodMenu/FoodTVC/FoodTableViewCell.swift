//
//  FoodTableViewCell.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class FoodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgFood: UIImageView!
    
    @IBOutlet weak var lbl_food_money: UILabel!
    
    @IBOutlet weak var lbl_food_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgFood.CircleImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDataForFood(_ image:String, lbl_name:String , lbl_money:String){
        imgFood.sd_setImage(with: URL(string: image))
        lbl_food_name.text = lbl_name
        lbl_food_money.text = lbl_money
    }
    
}
