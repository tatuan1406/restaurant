//
//  CircleImage.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/20/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

extension UIImageView{
    func CircleImage()  {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
    func BorderImage()  {
        self.layer.cornerRadius = 15
        self.layer.shadowColor = UIColor.gray.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 20).cgPath
    }
}
