//
//  ModelDrink.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/18/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ModelDrink:Mappable {
    var drink: [Drinks]?
    
    init?(map: Map){
        
    }
    
    mutating func mapping(map: Map) {
        drink <- map["drinks"]
    }
    
}
struct Drinks:Mappable {
    init?(map: Map) {
        
    }
    
    var idDrink: String? // id
    var strDrink: String? //ten
    var strGlass:String? //loai coc
    var strInstructions: String? // mo ta
    var strDrinkThumb: String? // hinh anh
    var strAlcoholic: String?
    var strCategory: String?
    
    mutating func mapping(map: Map) {
        idDrink <- map["idDrink"]
        strDrink <- map["strDrink"]
        strGlass <- map["strGlass"]
        strInstructions <- map["strInstructions"]
        strDrinkThumb <- map["strDrinkThumb"]
        strAlcoholic <- map["strAlcoholic"]
        strCategory <- map["strCategory"]
    }
}
