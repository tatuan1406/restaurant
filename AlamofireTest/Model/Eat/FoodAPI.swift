//
//  EatAPI.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya

enum FoodAPI{
    case keyword(food:String)
}

extension FoodAPI:TargetType{
    var baseURL: URL {
        return URL(string: "https://www.themealdb.com/")!
    }
    
    var path: String {
        switch self {
        case .keyword(_):
            return "api/json/v1/1/filter.php"
        }
    }
    
    //Get or Post
    var method: Moya.Method {
        switch self {
        case .keyword(_):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    //https://www.themealdb.com/api/json/v1/1/filter.php?c=Beef
    var task: Task {
        switch self {
        case .keyword(let food):
            return .requestParameters(parameters: ["c":food], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
