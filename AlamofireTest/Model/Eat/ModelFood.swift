//
//  ModelFood.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ModelFood:Mappable {
    var meals : [Food]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        meals <- map["meals"]
    }
}
struct Food:Mappable {
    var idMeal :String?
    var strMeal:String?
    var strMealThumb:String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        idMeal <- map["idMeal"]
        strMeal <- map["strMeal"]
        strMealThumb <- map["strMealThumb"]
    }
    
    
}
