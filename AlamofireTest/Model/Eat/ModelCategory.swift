//
//  ModelEat.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/21/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ModelCategory:Mappable {
    var categories:[Category]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        categories <- map["categories"]
    }
    
    
}

struct Category:Mappable {
    
    var idCategory:String?
    var strCategory:String?
    var strCategoryThumb:String?
    var strCategoryDescription:String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        idCategory <- map["idCategory"]
        strCategory <- map["strCategory"]
        strCategoryThumb <- map["strCategoryThumb"]
        strCategoryDescription <- map["strCategoryDescription"]
    }
    
  
}
