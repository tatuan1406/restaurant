//
//  PresenterCategory.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/21/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol DelegateCategory:class {
    func putDataCategory()
}

class PresenterCategory {
    
    weak var delegateCategory: DelegateCategory?
    
    convenience init(delegate: DelegateCategory) {
        self.init()
        self.delegateCategory = delegate
    }
    
    var countCategory:Int = 0
    var ArrayIDCat:[String] = []
    var ArraystrCategory:[String] = []
    var ArraystrCategoryThumb:[String] = []
    var ArraystrCategoryDescription :[String] = []
    
    func getDataCategory()  {
        Alamofire.request(API_Eat_Category).responseObject { (response: DataResponse<ModelCategory>) in
            self.countCategory = (response.result.value?.categories!.count)!
            print(self.countCategory)
            for i in 0..<self.countCategory{
                
                let idCategory = (response.value?.categories?[i].idCategory)!
                let strCategory = (response.value?.categories?[i].strCategory)!
                let strCategoryThumb = (response.value?.categories?[i].strCategoryThumb)!
                let strCategoryDescription = (response.value?.categories?[i].strCategoryDescription)!
                
                //
                self.ArrayIDCat.append(idCategory)
                self.ArraystrCategory.append(strCategory)
                self.ArraystrCategoryThumb.append(strCategoryThumb)
                self.ArraystrCategoryDescription.append(strCategoryDescription)
            }
            self.delegateCategory?.putDataCategory()
        }
    }
}
