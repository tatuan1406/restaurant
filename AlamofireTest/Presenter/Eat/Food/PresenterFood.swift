//
//  PresenterFood.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

protocol DelegateFood:class {
    func didGetData()
}
class PresenterFood{
    
    weak var delegateFood :DelegateFood?
    
    convenience init(delegate: DelegateFood) {
        self.init()
        self.delegateFood = delegate
    }
    var keyword:String?
    
    func getKeyword(keyword: String) {
        self.keyword = keyword.lowercased()
    }
    
    //moya get data
    let foodProvider = MoyaProvider<FoodAPI>()
    //cell model
    var foodModel : ModelFood?
    
    // func
    func getDataWithKeyword(){
        foodProvider.request(FoodAPI.keyword(food: keyword ?? "")) { (result) in
            switch result{
            case .success(let response):
                do{
                    let json = try response.mapJSON()
                    guard let dataJSON = Mapper<ModelFood>().map(JSONObject: json)
                        else {return}
                    self.foodModel = dataJSON
                    self.delegateFood?.didGetData()
                    
                }catch {
                    print("error)")
                }
            case .failure(_):
                print("error)")
            }
        }
    }
}
