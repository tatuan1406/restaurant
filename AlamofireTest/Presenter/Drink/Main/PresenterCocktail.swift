//
//  PresenterCocktail.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/18/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol DelegateCocktail: class {
    func getData()
}

class PresenterCocktail {
    
    weak var cocktailDelegate : DelegateCocktail?
    
    convenience init(delegate: DelegateCocktail) {
        self.init()
        self.cocktailDelegate = delegate
    }
    
    var countAPI:Int = 0
    var ArrayId:[String] = []
    var ArrayName :[String] = []
    var ArrayGlass :[String] = []
    var ArrayInstruction :[String] = []
    var ArrayThumb :[String] = []
    var ArrayAlcoholic :[String] = []
    var ArrayCategory: [String] = []
    
    func getDataFromDB() {
        Alamofire.request(API_Drink).responseObject { (response: DataResponse<ModelDrink>) in
            self.countAPI = (response.result.value?.drink!.count)!
            for i in 0..<self.countAPI {
                let id = (response.result.value?.drink?[i].idDrink)!
                let name = (response.result.value?.drink?[i].strDrink)!
                let glass = (response.result.value?.drink?[i].strGlass)!
                let instruction = (response.result.value?.drink?[i].strInstructions)!
                let thumb = (response.result.value?.drink?[i].strDrinkThumb)!
                let alcoholic = (response.result.value?.drink?[i].strAlcoholic)!
                let category = (response.result.value?.drink?[i].strCategory)!
                //add to array
                self.ArrayId.append(id)
                self.ArrayName.append(name)
                self.ArrayGlass.append(glass)
                self.ArrayInstruction.append(instruction)
                self.ArrayThumb.append(thumb)
                self.ArrayAlcoholic.append(alcoholic)
                self.ArrayCategory.append(category)
            }
            self.cocktailDelegate?.getData()
        }
    }
    
}
