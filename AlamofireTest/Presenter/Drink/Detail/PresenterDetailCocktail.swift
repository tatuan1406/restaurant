//
//  PresenterDetailCocktail.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/20/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation

protocol DelegateDetailCocktail:class {

}

class PresenterDetailCocktail{
    weak var delegateDetailCocktail : DelegateDetailCocktail?

    var Name :String?
    var Glass :String?
    var Instruction:String?
    var Thumb:String?
    var Alcoholic :String?
    var Category: String?
    
    convenience init(delegateDetail: DelegateDetailCocktail){
        self.init()
        self.delegateDetailCocktail = delegateDetail
    }
    
    
    func getDataForDetail(name: String, glass: String, instruction: String, thumb: String, alcoholic: String, category: String) {
        Name = name
        Glass = glass
        Instruction = instruction
        Thumb = thumb
        Alcoholic = alcoholic
        Category = category
    }
}
